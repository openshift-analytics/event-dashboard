#!/usr/bin/env python

"""app.py: This is an Event Stream Analytics Dashboard"""

__version__ = '0.2.1'

import os
import logging
import urllib3
import pickle

from flask import Flask, jsonify
from urllib.parse import urlparse
from kafka import KafkaProducer
from threading import Thread
from json import dumps, loads, JSONEncoder, JSONDecoder

from EventStreamProcessor import *

# Lets get our config from ENV
DEBUG_LOG_LEVEL = bool(os.getenv('DEBUG', False))
# OpenShift
OPENSHIFT_API_ENDPOINT = 'https://127.0.0.1:8443/oapi/v1/'
BEARER_TOKEN = os.getenv('BEARER_TOKEN', 'UNSET')

IGNORED_NAMESPACES = ['analytics', 'default', 'openshift',
                      'openshift-infra', 'kube-public', 'kube-system']

# this is my global memory
repository = {}

# and these are the prefixes of pods i should get
pod_filter = []

# I run an app, so that you can have a look at my memories
app = Flask(__name__)


class DashboardConsumer(EventStreamProcessorAndOpenShiftClient, Thread):
    """This EventStreamProcessor will link up git commit, build, imagestream, deployment, pod-set, host-set.
    """
    daemon = True

    def __init__(self, endpoint, token, topic, bootstrap_servers, group=None, target=None, name=None, args=(), kwargs=None):
        super(DashboardConsumer, self).__init__(
            endpoint, token, topic, bootstrap_servers)
        Thread.__init__(self, group=group, target=target, name=name)

        self.classlogger = logging.getLogger('DashboardConsumer')

        self.consumer.subscribe(
            [PODS_OBJECTS_TOPIC_NAME, DEPLOYMENTS_OBJECTS_TOPIC_NAME, BUILDS_COMPLETED_TOPIC_NAME])

    def run(self):
        self.consume()

    def consume(self):
        for message in self.consumer:
            o = message.value

            # we dont have a look at certain namespaces
            if o['metadata']['namespace'] in IGNORED_NAMESPACES:
                continue

            # so, lets see what has been deployed...
            if o['kind'] == 'DeploymentConfig':
                imageStreamTagDigest = ''

                for trigger in o['spec']['triggers']:
                    if trigger['type'] == 'ImageChange':
                        lastImageDigestTriggered = trigger[
                            'imageChangeParams']['lastTriggeredImage']
                        imageStreamTagDigest = lastImageDigestTriggered.split('@')[
                            1]

                self.classlogger.info("Found an imageStreamTagDigest for %s: %s" %
                                      (o['metadata']['name'], imageStreamTagDigest))

                replicationController = "%s-%s" % (
                    o['metadata']['name'], o['status']['latestVersion'])

                try:
                    repository[imageStreamTagDigest][
                        'lastImageDigestTriggeredDeployment'] = lastImageDigestTriggered
                    repository[imageStreamTagDigest][
                        'replicationControllerCreated'] = replicationController

                    # lets adjust our filter...
                    if replicationController not in pod_filter:
                        pod_filter.append(replicationController)
                except IndexError as i:
                    self.classlogger.error(i)
                    self.classlogger.error(o)
                except KeyError as k:
                    self.classlogger.warning(
                        "Cant find imageStreamTagDigest %s in repository" % imageStreamTagDigest)
                except Exception as e:
                    self.classlogger.error(e)

                if imageStreamTagDigest not in repository.keys():
                    repository[imageStreamTagDigest] = {}
                    repository[imageStreamTagDigest]['metadata'] = {}
                    repository[imageStreamTagDigest]['metadata'][
                        'namespace'] = o['metadata']['namespace']

                if 'podList' not in repository[imageStreamTagDigest].keys():
                    repository[imageStreamTagDigest]['podList'] = []

            elif o['kind'] == 'Build':
                # check if this is a binary build, if so continue, as we cant
                # figure out a git commit hash
                if not 'git' in o['spec']['source'].keys():
                    self.classlogger.warning(
                        "There was no git in that build -> it must be a binary build")
                    continue

                if o['spec']['output']['to']['kind'] == 'ImageStreamTag':
                    imageStreamTagName = o['spec']['output']['to']['name'].split(':')[
                        0]

                outputDockerImageReference = o['status'][
                    'outputDockerImageReference']

                imageStreamTagDigest = o['status'][
                    'output']['to']['imageDigest']

                if not imageStreamTagDigest in repository.keys():
                    repository[imageStreamTagDigest] = {}
                    repository[imageStreamTagDigest]['metadata'] = {}
                    repository[imageStreamTagDigest]['metadata'][
                        'namespace'] = o['metadata']['namespace']

                repository[imageStreamTagDigest][
                    'outputDockerImageReference'] = outputDockerImageReference
                repository[imageStreamTagDigest][
                    'imageStreamTagName'] = imageStreamTagName

                repository[imageStreamTagDigest][
                    'gitSource'] = o['spec']['source']['git']
                repository[imageStreamTagDigest]['gitSource'][
                    'commit'] = o['spec']['revision']['git']['commit']

                if 'podList' not in repository[imageStreamTagDigest].keys():
                    repository[imageStreamTagDigest]['podList'] = []
            elif o['kind'] == 'Pod':
                if o['metadata']['name'].endswith('-deploy') or o['metadata']['name'].endswith('-build'):
                    self.classlogger.debug(
                        "Just ignored Pod %s/%s ..." % (o['metadata']['namespace'], o['metadata']['name']))
                    continue

                # TODO take dying pods into account...
                # get Pod's imageStreamTagDigest
                try:
                    for cS in o['status']['containerStatuses']:
                        self.classlogger.debug("processing .status.containerStatusses of %s..." %
                                               o['metadata']['name'])
                        if 'running' in cS['state'].keys() and 'imageID' in cS.keys():
                            # TODO a pod may have more than one container...
                            istd = cS['imageID'].split('@')
                            if not istd == None or not istd == '':
                                imageStreamTagDigest = istd[1]
                except Exception as e:
                    logger.error(e)

                # check if this is one of the pods creaded by a deployment we
                # are analysing...

                if '-'.join(o['metadata']['name'].split('-')[0:-1]) in pod_filter:
                    der_hier = {}

                    der_hier['startTime'] = o['status']['startTime']
                    der_hier['name'] = o['metadata']['name']
                    der_hier['phase'] = o['status']['phase']

                    try:
                        der_hier['hostIP'] = o['status']['hostIP']
                        der_hier['podIP'] = o['status']['podIP']

                    except Exception as e:
                        self.classlogger.error(e)

                    # lets add the pod to out list...
                    try:
                        if o['metadata']['name'] not in repository[imageStreamTagDigest][
                                'podList']:
                            repository[imageStreamTagDigest][
                                'podList'].append(o['metadata']['name'])

                    except KeyError as k:
                        self.classlogger.error(k)
                        self.classlogger.info(o)
                    except Exception as e:
                        self.classlogger.error(k)


@app.route('/')
def app_index():
    return "Hello World"


@app.route('/repository')
def app_repository():
    return jsonify({'repository': repository, 'currentPodFilter': pod_filter})


if __name__ == '__main__':
    """The main function
    This will launch the main Spark/Kafka stream processor class.
    """

    if DEBUG_LOG_LEVEL:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
        urllib3.disable_warnings()

    logger = logging.getLogger(__name__)

    logger.info('initializing...')

    # if we can read bearer token from /var/run/secrets/kubernetes.io/serviceaccount/token use it,
    # otherwise use the one from env
    try:
        logger.debug(
            'trying to get bearer token from secrets file within pod...')
        with open('/var/run/secrets/kubernetes.io/serviceaccount/token') as f:
            BEARER_TOKEN = f.read()

        # if we can read bearer token from file, use openshift.svc as anedpoint
        # otherwise use localhost
        OPENSHIFT_API_ENDPOINT = 'https://openshift.default.svc.cluster.local/oapi/v1/'

    except:
        logger.info("not running within an OpenShift cluster...")

    logger.debug("Using %s and Bearer token %s" %
                 (OPENSHIFT_API_ENDPOINT, BEARER_TOKEN))

    dashboard_consumer = DashboardConsumer(
        OPENSHIFT_API_ENDPOINT, BEARER_TOKEN, 'openshift.raw',
        bootstrap_servers=os.getenv('KAFKA_PEERS', 'localhost:9092'))
    dashboard_consumer.start()

    app.run(host='0.0.0.0', port=os.getenv(
        'APP_PORT', 8080), debug=DEBUG_LOG_LEVEL)

# Problem

As a Platform Operator, I want to know which Pods have been started with a specific source code release, so that I can identify all the Pods and Hosts running this source code release.

# Limitations

State is not persistet, so when ever the event-dashboard pod is redeployed, state will be erased.

There might be timing issues, as the analyser expects builds to finish first, than a deployment should show up and afterwards pods... this could get mixed up in a real world.

# Development

Set up a Python virtualenv and install all the required python modules.

```
export BEARER_TOKEN=`oc whoami --show-token`
export APP_PORT=9865
./app.py
``` 

# Known Issues

* First Pod created right after Deloyment has started is missed....